import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { Router} from '@angular/router';
import { AppComponent } from './app.component';
import { PromoComponent1 } from './components/promo1.component';

import { RouterModule, Routes } from '@angular/router';
import { AppRoutingModule }  from './app-routing.module';
import {HttpModule, Http, URLSearchParams, Headers, RequestOptions} from '@angular/http';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './components/login.component'
import { HomeComponent } from './components/home.component'
import { ProductService } from './services/app.service';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import { DataService } from './services/data.service'
// import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

export const appRoutes: Routes = [

   { path:'loginPage',component: LoginComponent },
   { path:'homePage',component: HomeComponent,
    children: [
      { path: "", redirectTo: "tracks", pathMatch: "full" },
      { path: "firstPromotion", component: PromoComponent1 }
    
    ]
  },
];

@NgModule({
  declarations: [
    AppComponent,
    PromoComponent1,
    LoginComponent,
    HomeComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,FormsModule,
    RouterModule.forRoot(appRoutes,{useHash: true}),
    
  ],
  providers: [ProductService,DataService],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}
