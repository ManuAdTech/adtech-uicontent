export class ProductList {
    constructor(
        public ProductId: number,
        public ProductTitle: string,
        public imageUrl:string,
        public ScreenSize:string,
        public OperatingSystem:string,
        public StorageCapacity:string,
        public Features:string,
        public ProductType:string,
        public Description:string,
        public Author:string,
        public Language:string,
        public BookType:string,
        public Founder:string,
        public Industry:string,
        public Products:string,
        public UserId:string,
        public UserName:string,
        public Headquarters:string
     ){}


}
